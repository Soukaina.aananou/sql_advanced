---- Question 1 ----

-- Recherchons l'id Person de Landyn Greer --

select * from person where person_name='Landyn Greer';
-- son id est 1 --

-- Recherchons maintenant combien a-t-il eu de visites et à quelles dates et dans le même temps trouvons l'id des places qu'il a fréquenté --

select * from visit where person_id='1';

-- place_id=33 et 88 donc recherchons à quel endroit cela correspond -- 

select * from place where place_id in (33,88);

-- Donc Landyn greer a eu un séjour du 2020-09-26 18:28:00 au 2020-09-26 21:40:00 (il était au Grocery shop) et un second du 2020-10-07 20:54:00 au 2020-10-08 03:44:00 (il était au Park) --

SELECT person_name, health_status
FROM person p 
INNER JOIN visit v
ON p.person_id = v.person_id
WHERE (v.start_datetime, v.end_datetime) OVERLAPS ('2020-10-07 20:54:00','2020-10-08 03:44:00') AND v.place_id = 33
OR (v.start_datetime, v.end_datetime) OVERLAPS ('2020-09-26 18:28:00','2020-09-26 21:40:00') AND v.place_id = 88;

--------------------------------------

---- Question 2 ----

SELECT health_status, place_name, place_type
FROM visit v
INNER JOIN place pl
ON v.place_id = pl.place_id
INNER JOIN person p
ON v.person_id = p.person_id
WHERE health_status ='Sick'
AND place_type='Bar'
Order by place_name;

--- j'ai du mal a caler le moment donc les dates...---

---------------------------------------

---- Question 3 ----
-- Pour cette question j'ai créée une table pour pouvoir l'utiliser plus tard dans un select car comparer à landyn greer, Luna avait trop de visite. Cela va permettre de généraliser la requête. Donc pour se faire, j'ai ré utilisé un de mes codes d'un précédent stage où on prenait une table créé à partir d'une table déjà existante et nous faisions des recherches spécifiques (les miennes consister à rechercher les différents motifs de consultations en fonction des dysfonctions ostéopathiques retrouvées après la consultation. Combien de patients venaient à la même période pour tel motif et qu'avaient-ils comme réels dysfonctions au final). --
-- Ici je reprends toutes les données disponibles dans la table visit sauf person_id et visit_id qui ne sont pas utiles car on va préciser qu'on ne veut que les lignes de Taylor Luna avec une clause where person_id=4 (qui est l'id person de Luna). Ensuite nous pouvons faire la jointure avec un inner join à plusieurs. -- 

CREATE TABLE luna
AS (SELECT place_id,
	  start_datetime, 
	  end_datetime FROM visit WHERE person_id =4);


SELECT p.person_name, v.start_datetime, v.end_datetime
FROM person p
INNER JOIN visit v
ON p.person_id = v.person_id
INNER JOIN luna lu
ON lu.place_id = v.place_id
WHERE v.start_datetime <= lu.end_datetime AND v.end_datetime >= lu.start_datetime 
AND v.person_id !=4;

Drop table luna; -- une fois que nous n'en avons plus besoin --

---------------------------------------

-- Pour les deux questions qui suivent, afin de calculer la durée moyenne, il faut utiliser la fonction average (AVG) cependant avec une colonne en timestamp je n'ai pas trouvé de solution donc je n'ai pu avoir que le nombre de malades ou sain par endroit. Pour la fonction AVG, j'ai tenté plusieurs codes en changeant par mes données dont : SELECT AVG(DATE_FORMAT(StartDate,'%r')) FROM MyTable, SELECT AVG(StartDate::TIME) FROM MyTable, j'ai essayé avec avg(difftime(..., ...) mais rien n'a fonctionné -- 

---- Question 4 ----

SELECT health_status, place_name, v.place_id
FROM visit v
INNER JOIN person p
ON v.person_id = p.person_id
INNER JOIN place pl
ON v.place_id = pl.place_id
WHERE health_status ='Sick'
GROUP BY place_name, v.visit_id,pl.place_id,p.person_id
ORDER BY v.place_id asc;

---------------------------------------

---- Question 5 ----

SELECT health_status, place_name, v.place_id
FROM visit v
INNER JOIN person p
ON v.person_id = p.person_id
INNER JOIN place pl
ON v.place_id = pl.place_id
WHERE health_status ='Healthy'
GROUP BY place_name, v.visit_id,pl.place_id,p.person_id
ORDER BY v.place_id asc;





