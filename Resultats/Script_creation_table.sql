----- Suppression des tables 

DROP   TABLE  IF EXISTS visit;
DROP   TABLE  IF EXISTS place;
DROP   TABLE IF EXISTS person;

-- Création de la table person --

CREATE TABLE person (
person_id serial not null primary key,
person_name VARCHAR(100) NOT NULL,
health_status VARCHAR(100) NOT NULL,
confirmed_status_date TIMESTAMP
)
;

-- Création de la table place --

CREATE TABLE place (
place_id serial not null primary key,
place_name VARCHAR(100) NOT NULL,
place_type VARCHAR(100) NOT NULL
)
;

-- Création de la table visit --

CREATE TABLE visit (
visit_id serial not null primary key,
person_id INT NOT NULL REFERENCES person ON DELETE cascade,
place_id INT NOT NULL REFERENCES place,
start_datetime TIMESTAMP,
end_datetime TIMESTAMP,
FOREIGN KEY (person_id) REFERENCES person (person_id),
FOREIGN KEY (place_id) REFERENCES place (place_id)
)
;

set datestyle to "ISO, MDY;
